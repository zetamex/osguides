# Build Latest From Git Repos (unix)
**Written By:** *Timothy Francis Rogers*

### Prerequisites
Before you will be able to follow this tutorial you will need to know a few things already.
* Basic understanding of *unix systems
* Not be affraid of a commandline interface

### Install Git and Mono
This guide is going to assume you are going to be using a debian based distribution, and that you are wanting to use the version of git and mono that come with your distrobution.

First let us download the two main things we need to get the latest master dev build and the stuff to compile it. **note: you need to be a user with sudo/root priviledges**
````
sudo apt-get install git-core mono-complete
````

### Obtaining the Latest Dev Code
Now that we have installed the tools we need to get the job done, next we are going to clone on our machine the latest version of opensimulator from the github repository. The repository we will be cloning from is synced with the official one developers use every 10 minutes so it is a good place to pull from to not tax the server the developers use.

In your terminal window we are now going to clone the github repo using the following command:
````
git clone https://github.com/opensim/opensim.git
````

### Compiling the Latest Dev Code
We have now cloned the latest dev code to our machine, now we need to compile it. To do this we need to navigate into the folder it cloned too. We can do this by running the following command:
````
cd opensim
````
Now before we can actually build the project, we need to run the pre-build script wich will set all the proper flags for compiling and create a project file from which **xbuild** can use to compile the complete project. This is because opensim is made of several components, we have to make sure the project file knows to compile all of them together. To run the pre-build script run the following command: 
````
./runprebuild.sh
````
Now we can get to the actual compiling. Please note before compiling, depending on the power of your machine doing the compiling, this may take from a couple minutes to a good few minutes. This ofcourse depends on your machines hardware. But let us start the build by running the following command:
````
xbuild
````
Now after this completes, so long as you don't get anything that says **error** you should now have your fully self-compiled build of the latest development branch of opensimulator. **Note: You may see warnings, but it is very uncommon with how early in development opensim it is to compile without any warnings. The warnings are normally fine to ignore.**

### Use Your Newly Compiled Version of OpenSim
You can now successfully just navigate into the bin folder and run it. Please note however, this version from the repos are not preconfigured at all, you will need to manually setup your OpenSim.ini Grid/StandaloneCommon.ini files etc. But that is something that will be covered in a different tutorial.

##### Credits
This tutorial was writted by Timothy Francis Rogers, the former owner and founder of Zetamex which is now Zetamex Network. I Timothy now work for Zetamex Network, however not the owner anymore and now like to spearhead projects such as this to help push the growth and understanding of OpenSimulator.