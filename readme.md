# Git Hosted Tutorials

> Zetamex has published a bitbucket repo where you can add and contribute tutorials for OpenSimulator to the repo. This is designed to help promote how manage your own OpenSimulator virtual worlds.

## How To Contribute

### Articles We Want
* Setting up a standalone
* Setting up a grid
* Setting up a simulator
* Installing add-on modules
* Securing your set-up
* DNS Setup
* Reverse Proxy/Load Balancing
* Alternate OpenSim Version (ArribaSim, SimianGrid, etc)

### How can I contribute?
1. Clone this repo and add your own tutorials and submit a push request OR just email timothyrogers[at]zetamex[dot]com 
2. Make sure your tutorial is written in Markdown so that it displays correctly under sites like GitHub and Bitbucket as well as many others.
3. Be sure to follow the formatting guide.
4. Make sure you add the images you use, if you use any into the images folder for those who wish to download this repo and use offline.