# Sample Title
**Written By:** *Jonny Appleseed*

### Sample Step 1
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus interdum ac leo vitae blandit. Pellentesque eget sapien nisi. Nullam suscipit blandit sodales. Duis augue neque, suscipit nec pulvinar sed, scelerisque ut lacus. Praesent non leo bibendum, mollis diam at, mattis nibh. Duis nec nisl vitae risus bibendum pharetra. Integer rutrum metus sed lacinia efficitur. Nam lacinia sapien vel auctor porttitor. In in sem placerat, finibus est sit amet, varius dolor.
````
console commands like this
````
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus interdum ac leo vitae blandit. Pellentesque eget sapien nisi. Nullam suscipit blandit sodales. Duis augue neque, suscipit nec pulvinar sed, scelerisque ut lacus. Praesent non leo bibendum, mollis diam at, mattis nibh. Duis nec nisl vitae risus bibendum pharetra. Integer rutrum metus sed lacinia efficitur. Nam lacinia sapien vel auctor porttitor. In in sem placerat, finibus est sit amet, varius dolor.

### Title for next step
Make sure when adding images that you add alt text, and try to make sure you upload them to sites like imgur **and** this git repository so that it can easily be restored if the image is deleted from the online location it is linked from.
![alt text](http://i.imgur.com/7saJP7o.png "opensim logo")

#### Added Credits
Feel free to add small advertisement at the bottom to your site, grid, company, or business. This is after all a collaborative effort.